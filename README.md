# Conversor de Texto em Fala

Programa para converter texto em fala utilizando o [_Web Speech API_](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API).

![Tela inicial](screenshot.png)

Projeto publicado em: [https://franciscochaves.gitlab.io/conversor-de-texto-em-fala](https://franciscochaves.gitlab.io/conversor-de-texto-em-fala)
